var express = require('express');
var fs = require('fs');
var multer = require('multer');
var User = require('./models/user');
var fs = require('fs');

var registerDB = require('./models/register')

var app = express();
app.use(express.static(__dirname + "/public"));

var upload = multer({ dest: './public/uploads' })


var dateFormat = require('dateformat');

module.exports = function(app, passport) {
    var bodyParser = require('body-parser');
    app.use(bodyParser.json({ limit: '50mb' }));
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(bodyParser.json());


    app.get('/regis_sys', isLoggedIn_user, function(req, res) {

        res.sendFile('regis.html', {
            root: 'public'
        })
    });

    app.post('/registration', isLoggedIn_user, function(req, res) {
        var displayName = req.user.user.displayName;
        var album = req.body.album
        // console.log("ALBUM LENGTH::: " + album.length)
        var img = [];
        var process = 0;
        // console.log(album.length)
        album.forEach(function(photo) {
            var filename = (new Date).valueOf() + '.png';
            fs.writeFile('uploads/' + filename, photo, 'base64', function(err) {
                // console.log(process)
                if (err) {
                    console.log(err);
                }
                process++
                img.push({ 'name': photo });
                if (process == album.length) {
                    store(img);
                }
            });
        })

        function store(pictures) {
            var data = req.body.data;
            var now = new Date();
            var day = dateFormat(now, "dd-mm-yyyy");// HH:MM:ss 
            var time = dateFormat(now, "HH:MM:ss ");
            var newRegis = new registerDB();

            newRegis.number = data.number;
            newRegis.license = data.license;
            newRegis.address = data.prefix + "/" + data.suffix;
            // newRegis.suffix = data.suffix;
            newRegis.date = day
            newRegis.time = time
            newRegis.img = pictures;
            newRegis.by = displayName;

            newRegis.save(function(err, result) {
                if (err) {
                    console.log(err);
                }
                res.send("success")
            })
        }

    });

    app.get('/', function(req, res) {
        res.render('user_index.ejs');
    });

    app.get('/user_login', function(req, res) {
        res.render('user_login.ejs', { message: req.flash('loginMessage') });
    });

    app.post('/user_login', passport.authenticate('user-login', {
        successRedirect: '/regis_sys',
        failureRedirect: '/user_login',
        failureFlash: true
    }));

    app.get('/user_signup', function(req, res) {
        res.render('user_signup.ejs', { message: req.flash('signupMessage') });
    });


    app.post('/user_signup', passport.authenticate('user-signup', {
        successRedirect: '/regis_sys',
        failureRedirect: '/user_signup',
        failureFlash: true
    }));


    app.get('/user_logout', isLoggedIn_user, function(req, res) {
        req.logout();
        res.redirect('/');

    })



    //-----------------------------------ADMIN---------------------------------------------------
    app.get('/admin', function(req, res) {
        res.render('admin_index.ejs');
    });

    app.get('/admin_login', function(req, res) {
        res.render('admin_login.ejs', { message: req.flash('loginMessage') });
    });

    app.post('/admin_login', passport.authenticate('admin-login', {
        successRedirect: '/monitor',
        failureRedirect: '/admin_login',
        failureFlash: true
    }));

    /////
    app.get('/admin_signup', function(req, res) {
        res.render('admin_signup.ejs', { message: req.flash('signupMessage') });
    });


    app.post('/admin_signup', passport.authenticate('admin-signup', {
        successRedirect: '/monitor',
        failureRedirect: '/admin_signup',
        failureFlash: true
    }));

    app.get('/monitor', isLoggedIn_admin, function(req, res) {
        res.sendFile('monitor.html', {
            root: 'public'
        })
    });

    app.get('/monitorlist', isLoggedIn_admin, function(req, res) {
        registerDB.find(function(err, doc) {
            if (err) {
                console.log(err);
            } else {
                console.log(doc);
                res.send(doc);
            }
        })
    });

    app.get('/getCategories', isLoggedIn_admin, function(req, res) {
        User.find(function(err, doc) {
            if (err) {
                console.log(err);
            } else {
                console.log(doc);

                var list = [];
                doc.forEach(function(item) {
                    var name = item.user.displayName;
                    list.push(name)
                })
            }
            res.send(list);
        })
    });



    app.get('/view/:id', isLoggedIn_admin, function(req, res) {
        var id = req.params.id;
        registerDB.findOne({
            '_id': id
        }, function(err, doc) {
            if (err) {
                console.log(err);
            } else {
                res.send(doc);
            }
        })
    });


    app.get('/admin_logout', isLoggedIn_admin, function(req, res) {


        req.logout();
        res.redirect('/admin');

    })
};

function isLoggedIn_admin(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }

    res.redirect('/admin_login');
}

function isLoggedIn_user(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }

    res.redirect('/user_login');
}
