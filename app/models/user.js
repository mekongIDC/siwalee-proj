var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
var userSchema = mongoose.Schema({
	admin: {
		username: String,
		password: String
	},
	user: {
		username: String,
		displayName: String,
		password: String
	}
});

userSchema.methods.generateHash = function (password) {
	return bcrypt.hashSync(password, bcrypt.genSaltSync(9));
}

userSchema.methods.adminValidPassword = function (password) {
	return bcrypt.compareSync(password, this.admin.password);
}

userSchema.methods.userValidPassword = function (password) {
	return bcrypt.compareSync(password, this.user.password);
}

module.exports = mongoose.model('User', userSchema);