var mongoose = require('mongoose');

module.exports = mongoose.model('registerDB', {
    "number": Number,
    "license": Number,
    "address": String,
    "date": String,
    "time": String,
    "by": String,
    "img": [{
        name: String
    }]
});

