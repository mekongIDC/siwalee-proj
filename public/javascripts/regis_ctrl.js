
var app = angular.module('regisApp', []);
app.controller('regisController', function ($scope, $http, $window) {
    $scope.album = [];
    (function () {
        // The width and height of the captured photo. We will set the
        // width to the value defined here, but the height will be
        // calculated based on the aspect ratio of the input stream.

        var width = 1200;    // We will scale the photo width to this 320
        var height = 0;     // This will be computed based on the input stream

        // |streaming| indicates whether or not we're currently streaming
        // video from the camera. Obviously, we start at false.

        var streaming = false;

        // The various HTML elements we need to configure or control. These
        // will be set by the startup() function.

        var video = null;
        var canvas = null;
        var photo = null;
        var startbutton = null;

        function startup() {
            video = document.getElementById('video');
            canvas = document.getElementById('canvas');
            photo = document.getElementById('photo');
            startbutton = document.getElementById('startbutton');

            navigator.getMedia = (navigator.getUserMedia ||
                navigator.webkitGetUserMedia ||
                navigator.mozGetUserMedia ||
                navigator.msGetUserMedia);

            navigator.getMedia(
                {
                    video: true,
                    audio: false
                },
                function (stream) {
                    if (navigator.mozGetUserMedia) {
                        video.mozSrcObject = stream;
                    } else {
                        var vendorURL = window.URL || window.webkitURL;
                        video.src = vendorURL.createObjectURL(stream);
                    }
                    video.play();
                },
                function (err) {
                    console.log("An error occured! " + err);
                }
            );

            video.addEventListener('canplay', function (ev) {
                if (!streaming) {
                    height = video.videoHeight / (video.videoWidth / width);

                    // Firefox currently has a bug where the height can't be read from
                    // the video, so we will make assumptions if this happens.

                    if (isNaN(height)) {
                        height = width / (4 / 3);
                    }

                    video.setAttribute('width', width);
                    video.setAttribute('height', height);
                    canvas.setAttribute('width', width);
                    canvas.setAttribute('height', height);
                    streaming = true;
                }
            }, false);

            startbutton.addEventListener('click', function (ev) {
                takepicture();
                ev.preventDefault();
            }, false);

            clearphoto();
        }

        // Fill the photo with an indication that none has been
        // captured.

        function clearphoto() {
            var context = canvas.getContext('2d');
            context.fillStyle = "#AAA";
            context.fillRect(0, 0, canvas.width, canvas.height);

            var data = canvas.toDataURL('image/png');

            // photo.setAttribute('src', data);
        }

        // Capture a photo by fetching the current contents of the video
        // and drawing it into a canvas, then converting that to a PNG
        // format data URL. By drawing it on an offscreen canvas and then
        // drawing that to the screen, we can change its size and/or apply
        // other changes before drawing it.

        function takepicture() {
            var context = canvas.getContext('2d');
            if (width && height) {
                canvas.width = width;
                canvas.height = height;
                context.drawImage(video, 0, 0, width, height);

                var data = canvas.toDataURL('image/png');
                $scope.album.push(data);
                $scope.$apply();

            } else {
                clearphoto();
            }
        }

        // Set up our event listener to run the startup process
        // once loading is complete.
        window.addEventListener('load', startup, false);
    })();


    var images = [];


    $scope.save = function () {

        if (validateForm()) {
            $scope.album.forEach(function (image) {
                images.push(JSON.stringify(getBase64Image(image)));
            })

            var data = {
                "number": $scope.number,
                "license": $scope.license,
                "prefix": $scope.prefix,
                "suffix": $scope.suffix
            }

            var info = {
                'data': data,
                'album': images
            }

            $http({
                method: 'POST',
                data: info,
                url: '/registration'
            }).then(function successCallback(response) {
                if (response) {
                    console.log(response)
                    alert("ลงทะเบียนเสร็จสมบูรณ์")
                    $scope.number = ""
                    $scope.license = ""
                    $scope.prefix = ""
                    $scope.suffix = ""
                    $scope.album = [];
                    images = [];
                } else {
                    alert("ลงทะเบียนไม่สำเร็จ")
                }
            }, function errorCallback(response) {
                console.log("error");
            });
        }
    }

    $scope.reset = function () {
        $scope.number = ""
        $scope.license = ""
        $scope.prefix = ""
        $scope.suffix = ""
        $scope.album = [];
        images = [];
    }

    $scope.delete = function (pic) {
        for (var i = $scope.album.length - 1; i >= 0; i--) {
            if ($scope.album[i] == pic) {
                $scope.album.splice(i, 1);
            }
        }

    }

    function getBase64Image(base64string) {
        return base64string.replace(/^data:image\/(png|jpg);base64,/, "");
    }

    function validateForm() {
        var cardNumber = document.forms["regisForm"]["cardNumber"].value;
        var licensePlate = document.forms["regisForm"]["license"].value;
        var preAddrees = document.forms["regisForm"]["prefix"].value;
        var sufAddrees = document.forms["regisForm"]["suffix"].value;

        //*************edit here******************
        /*
        if (cardNumber == "") {
            alert("กรุณากรอกข้อมูลเลขที่บัตร");
            return false;
        }*/
        if (licensePlate == "") {
            alert("กรุณากรอกข้อมูลทะเบียนรถ");
            return false;
        }
        /*if ($scope.album.length == 0) {
            alert("กรุณาถ่ายรูป");
            return false;
        }
        if (preAddrees == "" || sufAddrees == "") {
            alert("กรุณากรอกข้อมูลบ้านเลขที่");
            return false;
        }*/ else {
            return true;
        }
    }

});
