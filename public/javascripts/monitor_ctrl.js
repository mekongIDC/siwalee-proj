
var app = angular.module('monitorApp', []);
app.controller('monitorController', function ($scope, $http) {

    $http.get('/monitorlist').then(function (res) {
        // console.log(res.data);
        var reverse = res.data;
        $scope.data = reverse.reverse()
    })

    $scope.view = function (id) {
        console.log($scope.search)
        // console.log(id);
        $http.get('/view/' + id).then(function (res) {
            // console.log(res.data);
            $scope.info = res.data;
        })
    }

    $("#abc").datepicker({ dateFormat: "dd-mm-yy" });

    $scope.getDate = function () {
        // console.log($("#abc").val())
        $scope.date = $("#abc").val();
    }

    $scope.reset = function () {
        // console.log($("#abc").val())
        $scope.search = [];
        $scope.date = "";
    }

    var getCategories = function () {
        $http.get('/getCategories').then(function (res) {
            var list = res.data;
            // console.log(list);
            $scope.categories = clean(list);//clean(list);
        })
    }
    getCategories();

    var clean = function (list) {
        for (var i = 0; i < list.length; i++) {
            if (list[i] == null) {
                list.splice(i, 1);
                i--;
            }
        }
        return list;
    };

});
